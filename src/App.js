import React, { Component } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Login from './containers/login/login'
import Chat from './containers/chat/chat'
import PropTypes from 'prop-types'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      isAuthenticated: localStorage.getItem('token')
    }
  }

  render () {
    
    return (
      <div>
        <Switch>
          <Route path="/login" component={Login} />
          <Route path="/chat" component={Chat} />
          <Redirect to="/login" />
        </Switch>
      </div>
    );
  }

  static contextTypes = {
    router: PropTypes.object
  }
}

export default App
