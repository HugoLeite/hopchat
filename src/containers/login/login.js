import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import './login.css'

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            senha: '',
            username: localStorage.getItem('username'),
            token: localStorage.getItem('token')
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this)
        this.handleChangeSenha = this.handleChangeSenha.bind(this)
    }

    componentDidMount() {
        if (this.state.token) {
            this.context.router.history.push(`/chat`)
        }
    }

    handleChangeEmail(event) {
        this.setState({email: event.target.value})
    }

    handleChangeSenha(event) {
        this.setState({senha: event.target.value})
    }

    login = () => {
        const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if (!this.state.email) {
            alert('Informe o e-mail!')
        } else if (!this.state.senha) {
            alert('Informe a senha')
        } if (!pattern.test( this.state.email )) {
            alert('Informe um e-mail válido!')
        } else {
            const authData = {
                username: this.state.email,
                password: this.state.senha
            };
            
            axios.post('https://react-challenge-backend.mybluemix.net/api/auth', authData)
                .then(response => {
                    console.log(response);
                    localStorage.setItem('token', response.data.token);
                    localStorage.setItem('username', this.state.email);
                    this.context.router.history.push(`/chat`)
                })
                .catch(err => {
                    alert(err.response.data.message)
                })
        }
    }

    render() {
        return (
            <div className='login'>
                <div className='login-text'>
                    <p>Login</p>
                </div>
                <div>
                    <input type="email" className='login-values' placeholder="E-mail" 
                    value={this.state.email} 
                    onChange={this.handleChangeEmail}></input>
                    <input type="password" className='login-values' placeholder="Senha" 
                    value={this.state.senha}
                    onChange={this.handleChangeSenha}></input>
                </div>
                <div className="login-button">
                    <button onClick={() => {this.login()}}>Login</button>
                </div>
            </div>
        )
    }

    static contextTypes = {
        router: PropTypes.object
    }
}

export default Login