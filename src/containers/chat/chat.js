import React, { Component } from 'react'
import axios from 'axios'
import PropTypes from 'prop-types'
import './chat.css'

class Chat extends Component {
    constructor(props) {
        super(props)
        this.state = {
            username: localStorage.getItem('username'),
            token: localStorage.getItem('token'),
            messages: [],
            message: ''
        }
        const data = {
            input: {text: ""},
            context:{}
        }
        axios.post('https://react-challenge-backend.mybluemix.net/api/message',
            data, {
                headers: {'Authorization' : this.state.token}
            }
         ).then(returnApi => {
            this.state.messages.push({
                from: 'Chatbot',
                text: returnApi.data.output.text,
                context: returnApi.data.context
            })
            this.setState({messages: this.state.messages})
        }).catch(err => {
            alert(err.response.data.message)
        })
        this.handleChangeMessage = this.handleChangeMessage.bind(this)
    }

    componentDidMount() {
        if (!this.state.token) {
            this.context.router.history.push(`/login`)
        }
    }

    componentDidUpdate() {
        var messageBody = document.querySelector('#messageBody');
        messageBody.scrollTop = messageBody.scrollHeight - messageBody.clientHeight;
    }

    handleChangeMessage(event) {
        this.setState({message: event.target.value})
    }

    sendMessage = () => {
        const data = {
            input: {text: this.state.message},
            context:this.state.messages[this.state.messages.length - 1].context
        }
        const messages = this.state.messages
        messages.push({
            from: this.state.username,
            text: this.state.message
        })
        this.setState({messages: this.state.messages, message: ''})
        axios.post('https://react-challenge-backend.mybluemix.net/api/message',
            data, {
                headers: {'Authorization' : this.state.token}
            }
         ).then(returnApi => {
            messages.push({
                from: 'Chatbot',
                text: returnApi.data.output.text,
                context: returnApi.data.context
            })
            this.setState({messages: this.state.messages})
        }).catch(err => {
            alert(err.response.data.message)
        })
    }

    logout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        this.context.router.history.push(`/login`)
    }

    render() {
        return (
            <div className="chat">
                <div className="head">
                    <div className="title"><p>HopChat</p></div>
                    <div className="btn" onClick={this.logout}><button>Sair</button></div>
                </div>
                <div className="body" id="messageBody">
                    {this.state.messages.map((message, i) => {
                        if (message.from === 'Chatbot') {
                            return (message.text.map((text, index) => {
                                return (
                                    <div key={index} className='from-bot'>
                                        <span className='from'>{message.from}</span>
                                        <p className='text'>{text}</p>
                                    </div>)
                            }))
                        } else {
                            return (
                                <div key={i} className='from-user'>
                                    <span className='from'>{message.from}</span>
                                    <p className='text'>{message.text}</p>
                                </div>)
                        }
                    })}
                </div>
                <div className="footer">
                    <input type="text" placeholder="Mensagem" 
                    value={this.state.message} 
                    onChange={this.handleChangeMessage}/>
                    <button onClick={this.sendMessage}>Enviar</button>
                </div>
            </div>
        )
    }

    static contextTypes = {
        router: PropTypes.object
    }
}

export default Chat